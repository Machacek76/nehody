const accidentSchema = new Schema({
    id: String,
    sender: String,
    timeGen: Date,
    timeSta: Date,
    timeSto: Date,
    timeUpd: Date,
    message: String
});

const accidentModel = mongoose.model('accidents', accidentSchema);