const accidents = require('./controller/accidents');



exports.routesConfig = function(app) {

    // INDEX
    app.get('/', (req, res) => {
        res.send('Hello World');
    });

    app.get('/accidents', (req, res) => {
        res.setHeader('content-type', 'application/json');
        accidents.get()
            .then(_res => {
                res.send(_res)
            });
    });

    app.get('/accidents/load', (req, res) => {
        res.setHeader('content-type', 'application/json');
        accidents.load()
            .then(_res => {
                res.send(_res)
            });
    });

    app.get('*', function(req, res) {
        res.status(404).send('what???');
    });
};