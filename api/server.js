'use strict';

const dotenv = require('dotenv');
const express = require('express');
const fs = require('fs');
const Router = require('./routers');

if (!process.env.PORT) {
    dotenv.config({ path: ".env" });
}

// Constants
const PORT = process.env.PORT;
const HOST = process.env.HOST;

// Create temp folder
const tempDir = __dirname + '/' + process.env.TEMP;
global.TMP = tempDir;

fs.exists(tempDir, (ex) => {
    if (!ex) {
        fs.mkdirSync(tempDir, { mode: 0o777 });
    }
})

// App
const app = express();
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Credentials', 'true');
    res.header('Access-Control-Allow-Methods', 'GET,HEAD,PUT,PATCH,POST,DELETE');
    res.header('Access-Control-Expose-Headers', 'Content-Length');
    res.header('Access-Control-Allow-Headers', 'Accept, Authorization, Content-Type, X-Requested-With, Range');

    if (req.method === 'OPTIONS') {
        return res.sendStatus(200);
    } else {
        return next();
    }
});

Router.routesConfig(app);

//app.listen(PORT, HOST);
//console.log(`Running on http://${HOST}:${PORT}`);

module.exports = app.listen(PORT, HOST);