const request = require('supertest');
const fs = require('fs');
const path = require('path');
const should = require('should');
const axios = require('axios');
const MockAdapter = require('axios-mock-adapter');
const setting = require('../setting');
const app = require('../../server');
const accidents = require('../../controller/accidents');

afterEach(() => {
    // nastavime temp pro testy
    global.TMP = __dirname + '/../tmp';

    // smazeme vsechny temp file
    fs.readdir(__dirname + '/../tmp', (err, files) => {
        if (err) throw err;
        for (const file of files) {

            if (file !== 'accidents.json') {
                fs.unlink(path.join(__dirname + '/../tmp', file), err => {
                    if (err) throw err;
                });
            }
        }
    });
});

describe('GET /accidents', () => {
    it('responds with json', (done) => {

        // sice nacitame z tmp ale potrebojeme vrati konkretni json
        global.TMP = __dirname + '/fixture';

        request(app)
            .get('/accidents')
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect((res) => {
                res.body.should.containEql({
                    test: "test json"
                });
            })
            .expect(200, done);
    });

    it('responds with not found', (done) => {
        console.log('================================');
        // nechceme aby se neco nacetlo a dostali jsme not found
        global.TMP = __dirname + '/fake-folder';

        request(app)
            .get('/accidents')
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect((res) => {
                res.body.should.containEql({
                    error: "No results"
                });
            })
            .expect(200, done);
    });
});

describe('GET /accidents/load should load data from web policiecr', () => {

    it('responds with json', (done) => {

        request(app)
            .get('/accidents/load')
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect((res) => {

                fs.readFile(global.TMP + '/accidents.json', 'utf-8', (err, _res) => {
                    res.body.should.containEql(JSON.parse(_res));
                });
            })
            .expect(200, done);
    });
});


describe('GET /accidents/  bad response', () => {

    it('responds with bad xml response', (done) => {

        let mock = new MockAdapter(axios);
        mock.onGet('https://aplikace.policie.cz/dopravni-informace/GetFile.aspx')
            .reply(200);

        accidents.load()
            .catch(error => {
                done();
            });
    });

    it('responds with 500', (done) => {

        let mock = new MockAdapter(axios);

        mock.onGet('/fake-url')
            .reply(500);
        accidents.load()
            .then(() => {
                done();
            })
            .catch(error => {
                done();
            });
    });
})