const request = require('supertest');
const fs = require('fs');
const path = require('path');
const should = require('should');
const assert = require('assert');
const setting = require('./setting');
const app = require('../server');


describe('GET /', () => {
    it('should get /', (done) => {

        request(app)
            .get('/')
            .set('Accept', 'text/html')
            .expect('Content-Type', /html/)
            .expect((res) => {
                res.text.should.equal('Hello World');
            })
            .expect(200, done);
    })
});

describe('GET /blah e404', () => {
    it('should get /', (done) => {

        request(app)
            .get('/blah')
            .set('Accept', 'text/html')
            .expect('Content-Type', /html/)
            .expect((res) => {
                res.text.should.equal('what???');
            })
            .expect(404, done);
    });
});

describe('OPTIONS request', () => {
    it('should get / OPTIONS', (done) => {

        request(app)
            .options('/')
            .set('Accept', 'text/plain')
            .expect((res) => {
                res.text.should.equal('OK');
            })
            .expect(200, done);
    });
});