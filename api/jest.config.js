module.exports = {

    //    preset: '@vue/cli-plugin-unit-jest/presets/no-babel',

    // transform: {
    //     '^.+\\.vue$': 'vue-jest'
    // },

    transform: {
        "^.+\\.(js|ts)$": "ts-jest",
    },

    testMatch: [
        "**/tests/unit/**/*.spec.[jt]s?(x)", "**/tests/**/*.test.[jt]s?(x)"
    ],

}