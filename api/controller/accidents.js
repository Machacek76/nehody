const xml2js = require('xml2js');
const fs = require('fs');
const axios = require('axios');

const dataUrl = 'https://aplikace.policie.cz/dopravni-informace/GetFile.aspx';
const accidentsName = '/accidents.json';

/**
 * Nacte XML data z webu policiecr.cz a ulozi jako json
 * @param {request} req 
 * @param {response} res 
 * @returns string
 */
exports.load = (req, res) => {
    return new Promise((resolve, reject) => {
        axios
            .get(dataUrl)
            .then(res => {
                xml2js.parseString(res.data, (err, result) => {
                    if (err) {
                        reject(err);
                    }
                    fs.writeFile(global.TMP + accidentsName, JSON.stringify(result), (err) => {});
                    resolve(result);
                });
            })
            .catch(err => {
                resolve(`{ "error": "data not found", "url": "${dataUrl}" }`);
            });
    });
}

/**
 * Vrati lokalni json s dopravnim prehledem
 * @param {request} req 
 * @param {response} res 
 * @returns string
 */
exports.get = (req, res) => {
    return new Promise((resolve, reject) => {

        fs.readFile(global.TMP + accidentsName, 'utf-8', (err, res) => {
            if (err) {
                resolve({ error: 'No results' });
            }

            resolve(res);
        });

    });
}